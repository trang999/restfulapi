using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using RESTfulAPI.Models;

namespace RESTfulAPI.Views.Home
{
    public class ProductsModel : PageModel
    {
        private readonly IHttpClientFactory _clientFactory;

        public List<Product> Products { get; private set; }

        public ProductsModel(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task OnGetAsync()
        {
            var client = _clientFactory.CreateClient();

            var response = await client.GetAsync("http://localhost:5080/products");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Products = JsonConvert.DeserializeObject<List<Product>>(content);
            }
            else
            {
                Products = new List<Product>();
            }
        }
    }

}
